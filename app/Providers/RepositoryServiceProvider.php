<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
//        $models= [
//            "Unit"
//        ];
//
//
//        foreach ($models as $model) {
//            $this->app->bind(
//                "App\Repositories\Interfaces\{{ $model }}Interface",
//                "App\Repositories\{{ $model }}"
//            );
//        }

        $this->app->bind("App\Repositories\Interfaces\UnitInterface", 'App\Repositories\BaseRepository');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
