<?php

namespace App\Repositories\Interfaces;

interface UnitInterface
{
    public function all();

    public function getById($id);

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function with($relation);
}
