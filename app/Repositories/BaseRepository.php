<?php


namespace App\Repositories;

use App\Repositories\Interfaces\UnitInterface;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements UnitInterface
{
    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->latest();
    }

    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function create(array $data)
    {
        return $this->create($data);
    }

    public function update(array $data, $id)
    {
        $model = $this->getById($id);

        return tap($model)->update($data);
    }

    public function delete($id)
    {
        $model = $this->getById($id);
        try {
            return $model->delete();
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function with($relation)
    {
        return $this->model->with($relation);
    }
}
