<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <title>{{ env('APP_NAME', 'Perfect') }}</title>
    <link href="{{ url('css/index.css') }}" rel="stylesheet">
</head>

<body class="app">
    <div id="loader">
        <div class="spinner"></div>
    </div>
    <div>
        @include('layouts.sidebar')
        <div class="page-container">
            @include('layouts.header')
            <main class="main-content bgc-grey-100">
                <div id="mainContent">
                    <div class="full-container">
                        @yield('content')
                    </div>
                </div>
            </main>
            <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600">
                <span>Copyright © 2017 Designed by <a href="https://colorlib.com" target="_blank" title="Colorlib">Colorlib</a>. All rights reserved.</span>
            </footer>
        </div>
    </div>
    <script type="text/javascript" src="{{ url('js/index.js') }}"></script>
</body>

</html>