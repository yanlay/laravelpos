

import 'jquery';
import './masonry';
import './charts';
import './popover';
import './scrollbar';
import './search';
import './sidebar';
import './skycons';
import './chat';
import './datatable';
import './datepicker';
import './email';
import './fullcalendar';
import './utils';

window.addEventListener('load', function load() {
    const loader = document.getElementById('loader');
    setTimeout(function() {
        loader.classList.add('fadeOut');
    }, 300);
});
